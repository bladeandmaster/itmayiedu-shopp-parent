# 微服务电商系统项目搭建

## 一、itmayiedu-shopp-api

api接口层

## 二、itmayiedu-shopp-common

公共实体、常量、工具类的封装

## 三、itmayiedu-shopp-eurekaserver

eureka注册中心
```
server.port = 8761
```

## 三、itmayiedu-shopp-member

会员服务，启动依赖eureka注册中心、mysql数据库、redis、activemq
```
server.port = 8081
spring.application.name = member
```

## 四、itmayiedu-shopp-message

消息平台，启动依赖eureka注册中心、activemq
```
server.port = 8082
spring.application.name = message
```

## 五、itmayiedu-shopp-web

web层，实现QQ授权登陆，启动依赖eureka注册中心，
```
server.port = 8083
spring.application.name = web
```

## 六、itmayiedu-shopp-weixin

微信公众号服务，启动依赖eureka注册中心，需要部署在外围，微信服务器要回调本服务
```
server.port = 80
spring.application.name = weixin
```

创建消息模版  
创建地址：https://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index

支付成功通知  

恭喜您，在{{date.DATA}}支付了一笔{{money.DATA}}元，你的订单号为{{orderNumber.DATA}}
