# QQ登陆接入流程

## 一、网站接入流程
https://wiki.open.qq.com/wiki/website/%E7%BD%91%E7%AB%99%E6%8E%A5%E5%85%A5%E6%B5%81%E7%A8%8B

按照上述文档到 http://connect.qq.com/ 进行开发者注册，获得对应的appid与appkey

## 二、QQ授权登陆流程

api文档：https://wiki.connect.qq.com/%E4%BD%BF%E7%94%A8authorization_code%E8%8E%B7%E5%8F%96access_token

### 1、进行授权
https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=101420900&redirect_uri=http://127.0.0.1/qqLoginCallback
&state=123&scope=

用户登陆成功后，直接跳转地址http://127.0.0.1/qqLoginCallback?code=B758JHK234BLA&state=123，并把用户的授权码传回给开发者网站。
 
### 2、通过Authorization Code获取Access Token
https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id=101420900&redirect_uri=http://127.0.0.1/qqLoginCallback
&client_secret=bd56a336f6ac49a65005595c2a41201a&code=B758JHK234BLA&redirect_uri=http://127.0.0.1/qqLoginCallback

返回{access_token=FE04************************CCE2,expires_in=7776000,refresh_token=88E4************************BE14}

### 3、使用Access Token获取用户OpenID
https://graph.qq.com/oauth2.0/me?access_token=FE04SD0908CCE2

### 三、QQ授权登陆SDK安装
腾讯开放平台    
https://wiki.open.qq.com/

下载Sdk4J.jar  
https://wiki.open.qq.com/wiki/website/SDK%E4%B8%8B%E8%BD%BD

安装到本地maven仓库  
bin/mvn install:install-file -Dfile=sdk/Sdk4J.jar -DgroupId=com.sdk4j -DartifactId=sdk4j -Dversion=1.0 -Dpackaging=jar

### 项目中QQ登陆流程

/locaQQLogin进行授权(直接跳转到https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=101420900&redirect_uri=http://127.0.0.1/qqLoginCallback&state=123&scope=)
 
—>用户登陆成功 -> 浏览器直接跳转/qqLoginCallback(得到Authorization Code，使用授权码去获取token;使用Access Token获取用户OpenID) 

->/qqRelation(将OpenID与数据库的用户id进行绑定，存储到mb_user中)
