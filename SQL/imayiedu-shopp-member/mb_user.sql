CREATE TABLE `mb_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码，加密存储',
  `phone` varchar(20) DEFAULT NULL COMMENT '注册手机号',
  `email` varchar(50) DEFAULT NULL COMMENT '注册邮箱',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `phone` (`phone`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='用户表';

#增加openid字段
ALTER TABLE mb_user ADD COLUMN `openid` varchar(32) DEFAULT NULL COMMENT '用户openid';

INSERT  INTO `mb_user`  (username,password,phone,email,created,updated)
VALUES ('yushengjun3', 'e10adc3949ba59abbe56e057f20f883e', '15527339674', 'aa1@a', '2015-04-06 17:03:55', '2015-04-06 17:03:55');

